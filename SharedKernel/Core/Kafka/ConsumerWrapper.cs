using Confluent.Kafka;

namespace SharedKernel.Core.Kafka
{
    public class ConsumerWrapper
    {
        private string _topicName;
        private readonly IConsumer<string,string> _consumer;

        public ConsumerWrapper(ConsumerConfig config,string topicName)
        {
            _topicName = topicName;
            _consumer = new ConsumerBuilder <string,string>(config).Build();
            _consumer.Subscribe(topicName);
        }
        public string ReadMessage()
        {
            var consumeResult = _consumer.Consume();
            return consumeResult.Message.Value;
        }
    }
}