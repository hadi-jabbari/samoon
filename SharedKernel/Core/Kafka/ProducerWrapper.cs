using Confluent.Kafka;
using System;
using System.Threading.Tasks;

namespace SharedKernel.Core.Kafka
{
    public class ProducerWrapper
    {
        private readonly string _topicName;
        private readonly IProducer<string,string> _producer;
        private static readonly Random Rand = new Random();

        public ProducerWrapper(ProducerConfig config,string topicName)
        {
            _topicName = topicName;
            _producer = new ProducerBuilder<string,string>(config).Build();
        }
        public async Task WriteMessage(string message)
        {
            var dr = await _producer.ProduceAsync(_topicName, new Message<string, string>()
            {
                Key = Rand.Next(5).ToString(),
                Value = message
            });
            Console.WriteLine($"KAFKA => Delivered '{dr.Value}' to '{dr.TopicPartitionOffset}'");
            return;
        }
    }
}