﻿using System;

namespace SharedKernel.Core
{
    public class BusinessException : Exception
    {
        public long Code { get; private set; }
        public BusinessException(long code, string message) : base(message)
        {
            Code = code;
        }
    }
}
