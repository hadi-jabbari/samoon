﻿using System;

namespace SharedKernel.Core
{
    public class DomainEvent : IDomainEvent
    {
        public Guid EventId { get; }
        public DateTime EventPublishDateTime { get; }

        public DomainEvent()
        {
            EventId = Guid.NewGuid();
            EventPublishDateTime = DateTime.Now;
        }
    }
}