﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedKernel.Core
{
    public interface IDomainEvent
    {
        Guid EventId { get; }
        DateTime EventPublishDateTime { get; }
    }
}
