﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedKernel.Core
{
    public interface IUnitOfWork
    {
        void Begin();
        void Commit();
        void Rollback();
    }
}
