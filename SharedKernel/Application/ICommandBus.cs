﻿namespace SharedKernel.Application
{
    public interface ICommandBus
    {
        void Dispatch<T>(T command);
    }
}
