﻿using System.Collections.Generic;
using Framework.Domain;
using SharedKernel.Core;

namespace SharedKernel.Domain
{
    public abstract class AggregateRoot<T> : Entity<T>, IAggregateRoot
    {
        private List<IDomainEvent> _changes;
        protected AggregateRoot()
        {
            _changes = new List<IDomainEvent>();
        }
        public void Publish<TEvent>(TEvent @event) where TEvent : IDomainEvent
        {
            _changes.Add(@event);
        }
        public IReadOnlyList<IDomainEvent> GetChanges()
        {
            return _changes;
        }
        public void ClearChanges()
        {
            _changes.Clear();
        }
    }
}
