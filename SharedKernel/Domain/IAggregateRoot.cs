﻿using System.Collections.Generic;
using SharedKernel.Core;

namespace SharedKernel.Domain
{
    public interface IAggregateRoot
    {
        void Publish<TEvent>(TEvent @event) where TEvent : IDomainEvent;
        IReadOnlyList<IDomainEvent> GetChanges();
        void ClearChanges();
    }
}