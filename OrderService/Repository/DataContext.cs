using Microsoft.EntityFrameworkCore;
using OrderService.Domain;

namespace OrderService.Repository
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Buyer> Buyers { get; set; }
        public DbSet<Good> Goods { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>(mb =>
            {
                mb.HasKey(x => x.Id);
                mb.Property(x => x.BuyerId).IsRequired();
                mb.Property(x => x.Amount).IsRequired();
                mb.HasOne<Buyer>().WithMany(x => x.Orders).HasForeignKey(x => x.BuyerId);
                mb.HasMany<OrderItem>().WithOne(x => x.Order).HasForeignKey(x => x.OrderId);
            });

            modelBuilder.Entity<OrderItem>(mb =>
            {
                mb.HasKey(x => x.Id);
                mb.Property(x => x.ProductId).IsRequired();
                mb.Property(x => x.Quantity).IsRequired();
                mb.Property(x => x.Price).IsRequired();
            });

            modelBuilder.Entity<Buyer>(mb =>
            {
                mb.HasKey(x => x.Id);
                mb.Property(x => x.FullName).IsRequired();
            });
            modelBuilder.Entity<Good>(mb =>
            {
                mb.HasKey(x => x.Id);
                mb.Property(x => x.Name).HasMaxLength(512).IsRequired();
                mb.Property(x => x.Price).IsRequired();
                mb.HasOne(x => x.OrderItem).WithMany(x => x.Goods).HasForeignKey(x=>x.OrderItemId);
            });

        }
    }
}