using SharedKernel.Core;

namespace OrderService.Repository
{
    public class OrderUnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;

        public OrderUnitOfWork(DataContext context)
        {
            _context = context;
        }

        public void Begin()
        {
            _context.Database.BeginTransaction();
        }

        public void Commit()
        {
            _context.SaveChangesAsync();
            _context.Database.CommitTransaction();
        }

        public void Rollback()
        {
            _context.Database.RollbackTransaction();
        }
    }
}