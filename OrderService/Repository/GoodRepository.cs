﻿using OrderService.Domain;
using System.Threading.Tasks;

namespace OrderService.Repository
{
    public class GoodRepository : IGoodRepository
    {
        private readonly DataContext _context;
        public GoodRepository(DataContext context)
        {
            _context = context;
        }
        public async Task Add(Good good)
        {
            await _context.Goods.AddAsync(good);

        }
    }
}
