﻿using OrderService.Domain;
using System.Threading.Tasks;

namespace OrderService.Repository
{
    public interface IGoodRepository
    {
        Task Add(Good good);

    }
}
