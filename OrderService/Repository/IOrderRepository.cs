using System.Threading.Tasks;
using OrderService.Domain;

namespace OrderService.Repository
{
    public interface IOrderRepository
    {
        Task AddAsync(Order order);
    }
}