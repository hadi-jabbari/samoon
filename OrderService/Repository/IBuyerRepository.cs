using System.Threading.Tasks;
using OrderService.Domain;

namespace OrderService.Repository
{
    public interface IBuyerRepository
    {
        Task AddAsync(Buyer buyer);
        Task<bool> Exists(int customerId);
    }
}