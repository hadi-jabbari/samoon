using Microsoft.EntityFrameworkCore;
using OrderService.Domain;
using System.Threading.Tasks;

namespace OrderService.Repository
{
    public class BuyerRepository : IBuyerRepository
    {
        private readonly DataContext _context;

        public BuyerRepository(DataContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Buyer buyer)
        {
            await _context.Buyers.AddAsync(buyer);
        }

        public async Task<bool> Exists(int customerId)
        {
            return await _context.Buyers.AnyAsync(b => b.CustomerId == customerId);
        }
    }
}