using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SharedKernel.Core;

namespace OrderService.Repository
{
    public class UnitOfWorkPipelineBehavior<TRequest,TResponse> : IPipelineBehavior<TRequest,TResponse>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UnitOfWorkPipelineBehavior(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            Task<TResponse> response = null;
            try
            {
                _unitOfWork.Begin();
                response = next.Invoke();
                _unitOfWork.Commit();
            }
            catch (Exception e)
            {
                _unitOfWork.Rollback();
                Console.WriteLine(e);
            }
            return response;
        }
    }
}