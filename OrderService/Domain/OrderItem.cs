using Framework.Domain;
using System.Collections.Generic;

namespace OrderService.Domain
{
    public class OrderItem : Entity<int>
    {
        public OrderItem(int orderId, int productId, int quantity, decimal price)
        {
            OrderId = orderId;
            ProductId = productId;
            Quantity = quantity;
            Price = price;
        }

        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }
        public ICollection<Good> Goods { get; set; }
    }
}