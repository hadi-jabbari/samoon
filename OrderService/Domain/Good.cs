﻿using Framework.Domain;

namespace OrderService.Domain
{
    public class Good:Entity<int>
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int ProductId { get; set; }
        public OrderItem OrderItem { get; set; }
        public int OrderItemId { get; set; }
    }
}

