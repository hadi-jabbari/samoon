using System.Collections.Generic;
using Framework.Domain;

namespace OrderService.Domain
{
    public class Buyer : Entity<int>
    {
        public Buyer(int customerId, string fullName)
        {
            CustomerId = customerId;
            FullName = fullName;
        }

        public int CustomerId { get; set; }
        public string FullName { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}