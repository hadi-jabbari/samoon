using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.Domain;

namespace OrderService.Domain
{
    public class Order : Entity<int>
    {    
        private List<OrderItem> _items;
        public Order()
        {
        }
        public Order(int customerId)
        {
            BuyerId = customerId;
            _items = new List<OrderItem>();
        }
        public int BuyerId { get; set; }
        public Buyer Buyer { get; set; }
        public decimal Amount { get; set; }
        public IReadOnlyCollection<OrderItem> Items => new ReadOnlyCollection<OrderItem>(_items);

        public void AddItems(IList<OrderItem> items)
        {
            _items.AddRange(items);
        }
    }
}