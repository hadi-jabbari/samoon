﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderService.CustomeExcptions
{
    public class BuyerNotFoundException:Exception
    {
        public BuyerNotFoundException(string message):base(message)
        {

        }
    }
}
