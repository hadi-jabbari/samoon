using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OrderService.Domain;
using OrderService.Repository;

namespace OrderService.Query
{
    public class BuyersQuery : IRequest<IList<Buyer>>
    {
    }
    public class BuyerQuery : IRequest<Buyer>
    {
        public int Id { get; set; }
    }

    public class BuyersQueryHandler : IRequestHandler<BuyersQuery, IList<Buyer>>
    {
        private readonly DataContext _context;

        public BuyersQueryHandler(DataContext context)
        {
            _context = context;
        }
        public async Task<IList<Buyer>> Handle(BuyersQuery request, CancellationToken cancellationToken)
        {
            return await _context.Buyers.ToListAsync();
        }
    }

    public class BuyerQueryHandler : IRequestHandler<BuyerQuery, Buyer>
    {
        private readonly DataContext _context;

        public BuyerQueryHandler(DataContext context)
        {
            _context = context;
        }
        public async Task<Buyer> Handle(BuyerQuery request, CancellationToken cancellationToken)
        {
            return await _context.Buyers.SingleOrDefaultAsync(x => x.Id == request.Id);
        }
    }
}