using System.Threading;
using System.Threading.Tasks;
using MediatR;
using OrderService.CustomeExcptions;
using OrderService.Domain;
using OrderService.Repository;

namespace OrderService.Command
{
    public class AddBuyerCommand : IRequest<Task>
    {
        public int CustomerId { get; set; }
        public string FullName { get; set; }
    }

    public class AddBuyerCommandHandler : IRequestHandler<AddBuyerCommand, Task>
    {
        private readonly IBuyerRepository _repository;

        public AddBuyerCommandHandler(IBuyerRepository repository)
        {
            _repository = repository;
        }

        public async Task<Task> Handle(AddBuyerCommand request, CancellationToken cancellationToken)
        {
            //if (!await _repository.Exists(request.CustomerId))
                //throw new BuyerNotFoundException("There is no corresponding customer");

            var buyer = new Buyer(request.CustomerId, request.FullName);
            await _repository.AddAsync(buyer);
            return Task.CompletedTask;
        }
    }
}