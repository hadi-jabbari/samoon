using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using OrderService.CustomeExcptions;
using OrderService.Domain;
using OrderService.Repository;

namespace OrderService.Command
{
    public class AddOrderCommand : IRequest<Task>
    {
        public int CustomerId { get; set; }

        public List<Item> Items { get; set; }
        //public Order Order { get; set; }

        public class Item
        {

            public int ProductId { get; set; }
            public int Quantity { get; set; }
            public decimal Price { get; set; }
        }
    }

    public class AddOrderCommandHandler : IRequestHandler<AddOrderCommand, Task>
    {
        private readonly IOrderRepository _repository;
        private readonly IBuyerRepository _buyerRepository;

        public AddOrderCommandHandler(IOrderRepository repository, IBuyerRepository buyerRepository)
        {
            _repository = repository;
            _buyerRepository = buyerRepository;
        }

        public async Task<Task> Handle(AddOrderCommand request, CancellationToken cancellationToken)
        {
            //if (!await _buyerRepository.Exists(request.CustomerId))
                //throw new BuyerNotFoundException("There is no corresponding customer");

            var order = new Order(request.CustomerId);
            var orderItems = request.Items.Select(item => new OrderItem(0, item.ProductId, item.Quantity, item.Price)).ToList();
            order.AddItems(orderItems);
            await _repository.AddAsync(order);
            return Task.CompletedTask;
        }
    }
}