using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using MediatR;
using Newtonsoft.Json;
using SharedKernel.Core.Kafka;

namespace CustomerService.Event
{
    public class AddCustomerEvent : INotification
    {
        public int Id { get; set; }
        public string FullName { get; set; }
    }
    public class AddCustomerEventHandler : INotificationHandler<AddCustomerEvent>
    {
        private readonly ProducerConfig _config;
        public AddCustomerEventHandler(ProducerConfig config)
        {
            _config = config;
        }
        public async Task Handle(AddCustomerEvent notification, CancellationToken cancellationToken)
        {
            string serializedOrder =  JsonConvert.SerializeObject(notification);
            var producer = new ProducerWrapper(_config,"AddCustomerEvent");
            await producer.WriteMessage(serializedOrder);
        }
    }
}