using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CustomerService.Domain;
using CustomerService.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CustomerService.Query
{
    public class CustomersQuery : IRequest<IList<Customer>>
    {
    }
    public class CustomerQuery : IRequest<Customer>
    {
        public int Id { get; set; }
    }
    
    public class CustomersQueryHandler : IRequestHandler<CustomersQuery,IList<Customer>>
    {
        private readonly DataContext _context;

        public CustomersQueryHandler(DataContext context)
        {
            _context = context;
        }
        public async Task<IList<Customer>> Handle(CustomersQuery request, CancellationToken cancellationToken)
        {
            return await _context.Customers.ToListAsync();
        }
    }
    
    public class CustomerQueryHandler : IRequestHandler<CustomerQuery,Customer>
    {
        private readonly DataContext _context;

        public CustomerQueryHandler(DataContext context)
        {
            _context = context;
        }
        public async Task<Customer> Handle(CustomerQuery request, CancellationToken cancellationToken)
        {
            return await _context.Customers.SingleOrDefaultAsync(x => x.Id == request.Id);
        }
    }
}