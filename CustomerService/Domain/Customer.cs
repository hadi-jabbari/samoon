using Framework.Domain;

namespace CustomerService.Domain
{
    public class Customer : Entity<int>
    {
        
        public Customer(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
    }
}