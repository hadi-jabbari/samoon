using System.Threading;
using System.Threading.Tasks;
using CustomerService.Domain;
using CustomerService.Event;
using CustomerService.Repository;
using MediatR;

namespace CustomerService.Command
{
    public class AddCustomerCommand : IRequest<Task>
    {
        public Customer Customer { get; set; }
    }

    public class AddCustomerCommandHandler : IRequestHandler<AddCustomerCommand, Task>
    {
        private readonly ICustomerRepository _repository;
        private readonly IMediator _mediator;
        public AddCustomerCommandHandler(ICustomerRepository repository, IMediator mediator)
        {
            _repository = repository;
            _mediator = mediator;
        }

        public async Task<Task> Handle(AddCustomerCommand request, CancellationToken cancellationToken)
        {
            await _repository.AddAsync(request.Customer);
            await _mediator.Publish(new AddCustomerEvent { Id = request.Customer.Id, FullName = request.Customer.FullName }, cancellationToken);
            return Task.CompletedTask;
        }
    }
}