using CustomerService.Domain;
using Microsoft.EntityFrameworkCore;

namespace CustomerService.Repository
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) :base(options)
        {
        }
        
        public DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite("Data Source=Customer.db");
        } 
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>(mb =>
            {
                mb.HasKey(x => x.Id);
                mb.Property(x => x.FirstName).HasMaxLength(512).IsRequired();
                mb.Property(x => x.LastName).HasMaxLength(512).IsRequired();
                mb.Ignore(x => x.FullName);
            });

        }
    }
}