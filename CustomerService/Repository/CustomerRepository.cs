using System.Collections.Generic;
using System.Threading.Tasks;
using CustomerService.Domain;
using Microsoft.EntityFrameworkCore;

namespace CustomerService.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly DataContext _context;

        public CustomerRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<IList<Customer>> GetAsync()
        {
            return await _context.Customers.ToListAsync();
        }

        public async Task AddAsync(Customer customer)
        {
            await _context.Customers.AddAsync(customer);
            //TODO : below line is temporary and will be omitted soon
            _context.SaveChanges();
        }

        public async Task<Customer> GetAsync(int id)
        {
            return await _context.Customers.SingleOrDefaultAsync(x => x.Id == id);
        }
    }
}