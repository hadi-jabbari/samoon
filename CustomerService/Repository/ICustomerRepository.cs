using System.Collections.Generic;
using System.Threading.Tasks;
using CustomerService.Domain;

namespace CustomerService.Repository
{
    public interface ICustomerRepository
    {
        Task<IList<Customer>> GetAsync();
        Task<Customer> GetAsync(int id);
        Task AddAsync(Customer customer);
    }
}