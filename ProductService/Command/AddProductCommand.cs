﻿using MediatR;
using ProductService.Domain;
using ProductService.Repository;
using System.Threading;
using System.Threading.Tasks;

namespace ProductService.Command
{
    public class AddProductCommand : IRequest<Task>
    {
        public Product Product { get; private set; }
        public AddProductCommand(Product product)
        {
            Product = product;
        }
    }

    public class AddProductCommandHandler : IRequestHandler<AddProductCommand, Task>
    {
        private readonly IProductRepository _repository;

        public AddProductCommandHandler(IProductRepository repository)
        {
            _repository = repository;
        }
        public async Task<Task> Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            await _repository.Add(request.Product);
            return Task.CompletedTask;
            
        }
    }



}
