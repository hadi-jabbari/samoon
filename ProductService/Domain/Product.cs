using Framework.Domain;

namespace ProductService.Domain
{
    public class Product : Entity<int>
    {
        
        public Product(string name, decimal price)
        {
            Name = name;
            Price = price;
        }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}