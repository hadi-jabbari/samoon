using Microsoft.EntityFrameworkCore;
using ProductService.Domain;

namespace ProductService.Repository
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) :base(options)
        {
        }
        public DbSet<Product> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite("Data Source=Product.db");
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>(mb =>
            {
                mb.HasKey(x => x.Id);
                mb.Property(x => x.Name).HasMaxLength(512).IsRequired();
                mb.Property(x => x.Price).IsRequired();
            });
        }
    }
}