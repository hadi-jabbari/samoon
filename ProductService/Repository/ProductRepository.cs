using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProductService.Domain;

namespace ProductService.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly DataContext _context;

        public ProductRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IList<Product>> GetAsync()
        {
            return await _context.Products.ToListAsync();
        }
        public async Task<Product> GetAsync(int id)
        {
            return await _context.Products.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<decimal> GetPriceAsync(int id)
        {
            var product = await GetAsync(id);
            return product.Price;
        }

        public async Task Add(Product product)
        {
            await _context.Products.AddAsync(product);
        }
    }
}