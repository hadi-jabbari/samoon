﻿using SharedKernel.Core;

namespace ProductService.Repository
{
    public class ProductUnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;

        public ProductUnitOfWork(DataContext context)
        {
            _context = context;
        }

        public void Begin()
        {
            _context.Database.BeginTransaction();
        }

        public void Commit()
        {
            _context.SaveChangesAsync();
            _context.Database.CommitTransaction();
        }

        public void Rollback()
        {
            _context.Database.RollbackTransaction();
        }
    }
}
