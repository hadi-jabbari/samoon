using System.Collections.Generic;
using System.Threading.Tasks;
using ProductService.Domain;

namespace ProductService.Repository
{
    public interface IProductRepository
    {
        Task<IList<Product>> GetAsync();
        Task<Product> GetAsync(int id);
        Task<decimal> GetPriceAsync(int id);

        Task Add(Product product);
    }
}