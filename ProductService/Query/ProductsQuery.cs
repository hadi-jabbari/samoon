using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProductService.Domain;
using ProductService.Repository;

namespace ProductService.Query
{
    public class ProductsQuery : IRequest<IList<Product>>
    {
    }
    
    public class ProductQuery : IRequest<Product>
    {
        public int Id { get; set; }
    }
    

    public class ProductsQueryHandler : IRequestHandler<ProductsQuery, IList<Product>>
    {
        private readonly IProductRepository _repository;

        public ProductsQueryHandler(IProductRepository repository)
        {
            _repository = repository;
        }
        public async Task<IList<Product>> Handle(ProductsQuery request, CancellationToken cancellationToken)
        {
            return await _repository.GetAsync();
        }
    }

    public class ProductQueryHandler : IRequestHandler<ProductQuery, Product>
    {
        private readonly IProductRepository _repository;

        public ProductQueryHandler(IProductRepository repository)
        {
            _repository = repository;
        }
        public async Task<Product> Handle(ProductQuery request, CancellationToken cancellationToken)
        {
            return await _repository.GetAsync(request.Id);
        }
    }
}