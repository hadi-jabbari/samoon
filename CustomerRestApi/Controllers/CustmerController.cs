﻿using CustomerRestApi.Models;
using CustomerService.Command;
using CustomerService.Domain;
using CustomerService.Query;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CustomerRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustmerController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CustmerController(IMediator mediator)
        {
            _mediator = mediator;
        }


        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result =await _mediator.Send(new CustomersQuery ());
            return Ok(result);
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var result =await _mediator.Send(new CustomerQuery { Id = id });
            return Ok(result);
        }


        [HttpPost]
        public async Task<IActionResult> Add(AddCustomerModel customer)
        {
            var result = await _mediator.Send(new AddCustomerCommand { Customer = new Customer(customer.FirstName,customer.LastName) });
            return Ok();
        }

    }
}
