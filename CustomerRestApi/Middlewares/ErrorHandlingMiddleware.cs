﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace CustomerRestApi.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        public ILogger<ErrorHandlingMiddleware> _logger { get; }

        public ErrorHandlingMiddleware(RequestDelegate next,ILogger<ErrorHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }
        private  Task HandleExceptionAsync(HttpContext context, Exception ex)
        {

            var result = JsonConvert.SerializeObject(new { error = ex.Message });
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            _logger.LogError(result);
            return Task.CompletedTask;
        }
    }
}
