﻿using System.ComponentModel.DataAnnotations;

namespace CustomerRestApi.Models
{
    public class AddCustomerModel
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
    }
}
