﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProductRestApi.Models;
using ProductService.Command;
using ProductService.Domain;
using ProductService.Query;
using System.Threading.Tasks;

namespace ProductRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]

        public async Task<IActionResult> Get()
        {
            var result = await _mediator.Send(new ProductsQuery());
            return Ok(result);
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _mediator.Send(new ProductQuery { Id = id });
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddProductModel product)
        {
            await _mediator.Send(new AddProductCommand(new Product(product.Name, product.Price)));
            return Ok();
        }


    }
}
