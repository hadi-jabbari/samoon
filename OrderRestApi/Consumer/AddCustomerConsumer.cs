using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using MediatR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OrderService.Command;
using OrderService.Domain;
using SharedKernel.Core.Kafka;

namespace OrderRestApi.Consumer
{
    public class AddCustomerConsumer : BackgroundService
    {
        private readonly ConsumerConfig _config;
        private readonly IMediator _mediator;
        private readonly ILogger<AddCustomerConsumer> _logger;

        public AddCustomerConsumer(ConsumerConfig consumerConfig, IMediator mediator, ILogger<AddCustomerConsumer> logger)
        {
            _config = consumerConfig;
            _mediator = mediator;
            _logger = logger;
        }

        protected override Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                _logger.LogInformation("consuming...");
                var consumerHelper = new ConsumerWrapper(_config, "AddCustomerEvent");
                string addBuyerMessage = consumerHelper.ReadMessage();
                Buyer buyer = JsonConvert.DeserializeObject<Buyer>(addBuyerMessage);
                _logger.LogInformation($"consumed new customer {buyer.CustomerId}");
                _mediator.Send(new AddBuyerCommand { CustomerId = buyer.CustomerId, FullName = buyer.FullName });
            }
            return Task.CompletedTask;
        }
    }
}