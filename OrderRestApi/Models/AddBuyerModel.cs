﻿using System.ComponentModel.DataAnnotations;

namespace OrderRestApi
{
    public class AddBuyerModel
    {
        [Required]
        public int CustomerId { get; set; }
        [Required]
        public string FullName { get; set; }
    }
}
