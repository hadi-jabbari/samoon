﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OrderRestApi.Models
{
    public class OrderItemModel
    {
        [Required]
        
        public int ProductId { get; set; }
        [Required]
        [Range(0,10)]
        public int Quantity { get; set; }
        [Required]
        [Range(0, double.MaxValue)]
        public decimal Price { get; set; }
    }
}
