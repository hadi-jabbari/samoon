﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using OrderService.Command;
using OrderService.Query;
using System.Threading.Tasks;

namespace OrderRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BuyerController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BuyerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _mediator.Send(new BuyersQuery());
            return Ok(result);
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _mediator.Send(new BuyerQuery { Id = id });
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddBuyerModel buyer)
        {
            await _mediator.Send(new AddBuyerCommand { CustomerId = buyer.CustomerId, FullName = buyer.FullName });
            return Ok();
        }
    }
}
