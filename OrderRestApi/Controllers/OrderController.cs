﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using OrderService.Command;
using System.Threading.Tasks;

namespace OrderRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrderController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async  Task<IActionResult> Add(AddOrderCommand order)
        {
            var result =await _mediator.Send(order);
            return Ok();
        }


    }
}
